# Double-Spender
This tool allows you to generate two transactions using the same Bitcoins.

The first transaction will have a very low fee, and will only be accepted by a few nodes, but after some time it should make its way to the recipient.

The second transaction will have a generous fee and will be broadcast more easily to many nodes.



